# Shiny app Demo

This package launches a shiny app demo.
It contains only one function `lancer_mon_app()`

# Installation

`devtools::install_gitlab("henry-marylene/ExemplePackage", host = "https://gitlab.com/")`

# Use ExemplePackage

to launch the shiny app
`ExemplePackage::lancer_mon_app()`

Use `help(lancer_mon_app)` to see the documentation